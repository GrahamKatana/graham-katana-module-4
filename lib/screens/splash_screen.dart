import 'package:flutter/material.dart';
import 'package:m3_app/screens/login_screen.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Column(
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(24.4, 42.0, 24.4, 42.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // Image.asset("assets/back.png"),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 34.0),
          constraints: BoxConstraints.expand(
            height: 300.0,
          ),
          width: MediaQuery.of(context).size.width * 0.65,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage("assets/house.png"),
            fit: BoxFit.cover,
          )),
        ),
        Container(
          margin: EdgeInsets.only(top: 20.0),
          width: MediaQuery.of(context).size.width * 0.60,
          child: FittedBox(
            fit: BoxFit.fitHeight,
            child: Text(
              "Hello, Welcome",
              style: TextStyle(fontSize: 28.0),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 40.0),
          padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
          child: RaisedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            color: Colors.purple[300],
            textColor: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0)),
            padding: EdgeInsets.only(
                top: 15.0, bottom: 15.0, left: 15.0, right: 15.0),
            child: Text(
              "Get me my house".toUpperCase(),
              style: TextStyle(fontSize: 14),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20.0),
          child: Text(
            "Let's start",
            style: TextStyle(color: _colorFromHex('#737CA4'), fontSize: 18.0),
          ),
        )
      ],
    )));
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll("#", "");
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}
